#include <string>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <string>
#include <array>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;
using Eigen::MatrixXd;

int csvRead(MatrixXd& outputMatrix, const string& fileName, const streamsize dPrec) {
    ifstream inputData;
    inputData.open(fileName);
    cout.precision(dPrec);
    if (!inputData){
        cerr<<"No such file!"<<endl;
        return EXIT_FAILURE;
    }
    string fileline, filecell;
    unsigned int prevNoOfCols = 0, noOfRows = 0, noOfCols = 0;
    while (getline(inputData, fileline)) {
        noOfCols = 0;
        stringstream linestream(fileline);
        while (getline(linestream, filecell, ',')) {
            try {
                stod(filecell);
            }
            catch (...) {
                cerr<<"Data Error!"<<endl;
                return EXIT_FAILURE;
            }
            noOfCols++;
        }
        if (noOfRows++ == 0)
            prevNoOfCols = noOfCols;
        if (prevNoOfCols != noOfCols){
            cerr<<"Data Error!"<<endl;
            return EXIT_FAILURE;
        }
    }
    inputData.close();
    outputMatrix.resize(noOfRows, noOfCols);
    inputData.open(fileName);
    noOfRows = 0;
    while (getline(inputData, fileline)) {
        noOfCols = 0;
        stringstream linestream(fileline);
        while (getline(linestream, filecell, ',')) {
            outputMatrix(noOfRows, noOfCols++) = stod(filecell);
        }
        noOfRows++;
    }
    return 0;
}

void ReadDataFromCsv(string &filename, vector<vector<string> > &lines_feat) {
    ifstream vm_info(filename.c_str());
    if (!vm_info){
        cerr<<"No such file!"<<endl;
        exit(EXIT_FAILURE);
    }
    string lines, var;
    vector<string> row;

    lines_feat.clear();

    while(!vm_info.eof()) {
        getline(vm_info, lines);
        if(lines.empty())
            break;
        istringstream stringin(lines);
        row.clear();

        while(getline(stringin, var, ',')) {
            row.push_back(var);
        }
        lines_feat.push_back(row);
    }
}

void getUniverseVectors(vector<vector<string>> &universe, MatrixXd& ROR, MatrixXd& stdvar){
    int noOfPortofolio = universe.size();

    ROR.resize(noOfPortofolio, 1);
    stdvar.resize(noOfPortofolio, 1);

    for(int i = 0; i < noOfPortofolio; i++){
        try{
            ROR(i, 0) = stod(universe[i][1]);
            stdvar(i, 0) = stod(universe[i][2]);
        }
        catch(...){
            fprintf(stderr, "Data Error!");
            exit(EXIT_FAILURE);
        }
    }

}

MatrixXd getUnresX(int NoP, double targetReturn, MatrixXd& A, MatrixXd& sigma){
    MatrixXd b(2,1);
    b(0,0) = 1;
    b(1,0) = targetReturn;

    MatrixXd firstM((NoP+2),(NoP+2));
    firstM.block(0,0, NoP,NoP) = sigma;
    firstM.block(0,NoP,NoP,2) = A.transpose();
    firstM.block(NoP, 0, 2, NoP) = A;
    firstM.bottomRightCorner(2,2) = MatrixXd::Zero(2,2);

    MatrixXd rightM(NoP+2,1);
    rightM.block(0,0,NoP,1) = MatrixXd::Zero(NoP,1);
    rightM.bottomRightCorner(2,1) = b;

    MatrixXd unknown(NoP+2,1);
    unknown = firstM.colPivHouseholderQr().solve(rightM);

    MatrixXd x(NoP,1);
    x = unknown.block(0,0,NoP,1);

    return x;

}

double getStdVar(int NoP, double targetReturn, MatrixXd& A, MatrixXd& sigma){
    MatrixXd x(NoP, 0);
    x = getUnresX(NoP, targetReturn, A, sigma);
    return sqrt((x.transpose()*sigma*x)(0,0));
}

double resStdVar(int NoP, double targetReturn, MatrixXd& realA, MatrixXd& sigma){

    MatrixXd A(NoP, 2);
    A = realA;

    MatrixXd b(2,1);
    b(0,0) = 1;
    b(1,0) = targetReturn;

    MatrixXd x(NoP, 1);
    x = getUnresX(NoP, targetReturn, A, sigma);

    int indexOfNegative;
    for(int i = 0; i < NoP; i++){
        indexOfNegative = -1;
        if(x(i,0) < 0){
            indexOfNegative = i;
            break;
        }
    }

    if(indexOfNegative == -1){
        return getStdVar(NoP, targetReturn, A, sigma);
    }

    MatrixXd finalResX(NoP,1);

    while(indexOfNegative != -1){
        A.conservativeResize(A.rows()+1,A.cols());
        int m = A.rows();
        A.row(m-1) = MatrixXd::Zero(1,NoP);
        A(m - 1, indexOfNegative) = 1;

        b.conservativeResize(b.rows()+1, 1);
        b(b.rows()-1, 0) = 0;

        MatrixXd firstM(m + NoP,m + NoP);
        firstM.block(0,0, NoP, NoP) = sigma;
        firstM.block(0, NoP, NoP, m) = A.transpose();
        firstM.block(NoP, 0, m, NoP) = A;
        firstM.bottomRightCorner(m,m) = MatrixXd::Zero(m,m);

        MatrixXd rightM(NoP + m,1);
        rightM.block(0,0,NoP,1) = MatrixXd::Zero(NoP,1);
        rightM.bottomRightCorner(m,1) = b;

        MatrixXd unknown(NoP + m,1);
        unknown = firstM.colPivHouseholderQr().solve(rightM);

        MatrixXd resX(NoP,1);
        resX = unknown.block(0,0,NoP,1);

        for(int i = 0; i < NoP; i++){
            if(resX(i,0) < 0){
                indexOfNegative = i;
                break;
            }
            finalResX = resX;
            indexOfNegative = -1;
        }
    }
    return sqrt((finalResX.transpose()*sigma*finalResX)(0,0));
}

void printUnRes(int NoP, MatrixXd& A, MatrixXd& sigma){
    cout<<"ROR,volatility"<<endl;
    for (int i = 1; i < 27; i++){
        double targetReturn = i/100.0;
        double volatility = getStdVar(NoP, targetReturn, A, sigma) * 100;
        cout<<setprecision(1)<<fixed<<(double) i<< "%,";
        cout<<setprecision(2)<<fixed<<volatility<< "%" << endl;
    }
}

void printRes(int NoP, MatrixXd& A, MatrixXd& sigma){
    cout<<"ROR,volatility"<<endl;
    for (int i = 1; i < 27; i++){
        double targetReturn = i/100.0;
        double volatility = resStdVar(NoP, targetReturn, A, sigma) * 100;
        cout<<setprecision(1)<<fixed<<(double) i<< "%,";
        cout<<setprecision(2)<<fixed<<volatility<< "%" << endl;
    }
}

int main(int argc, char *argv[]){
    //argument                                                                                                                                                                                              
    if((argc != 3) && (argc != 4)){
        cerr << "Argument number error!" << endl;
        return EXIT_FAILURE;
    }

    string universe_csv, correlation_csv;
    if(argc == 3){
        universe_csv = argv[1];
        correlation_csv = argv[2];
    }

    if(argc == 4){
        string argv1 = argv[1];
        string argv2 = argv[2];
        string argv3 = argv[3];

        if(argv1 == "-r"){
            universe_csv = argv[2];
            correlation_csv = argv[3];
        }
        if(argv2 == "-r"){
            universe_csv = argv[1];
            correlation_csv = argv[3];
        }
        if(argv3 == "-r"){
            universe_csv = argv[1];
            correlation_csv = argv[2];
        }
        if((argv1 != "-r")&&(argv2 != "-r")&&(argv3 != "-r")){
            cerr<<"Argument error!"<<endl;
            return EXIT_FAILURE;
        }
    }

    //Reading csv into matrix                                                                                                                                                                               
    int error;
    MatrixXd Corr;
    error = csvRead(Corr, correlation_csv, 7);

    if(error != 0){
        cerr<<"Read fail!"<<endl;
        return EXIT_FAILURE;
    }

    vector<vector<string> > universe;
    string fileName1 = universe_csv;
    ReadDataFromCsv((string &) fileName1, universe);
    MatrixXd ROR, stdvar;

    getUniverseVectors(universe, ROR, stdvar);

    //get Matrix sigma                                                                                                                                                                                      
    if(ROR.rows() != Corr.rows() || stdvar.rows() != Corr.rows()){
        cerr<<"Inconsistent Data Dimensions!"<<endl;
        return EXIT_FAILURE;
    }
    MatrixXd sigma(Corr.rows(),Corr.cols());
    sigma = stdvar.asDiagonal() * Corr * stdvar.asDiagonal();

    int NoP = ROR.rows();
    MatrixXd A(2,NoP);
    for(int i = 0; i < NoP; i++){
        A(0,i) = 1;
        A(1,i) = ROR(i,0);
    }
    if(argc == 3){
        printUnRes(NoP,A,sigma);
    }

    if(argc == 4){
        printRes(NoP, A, sigma);
    }
    return 0;
}
